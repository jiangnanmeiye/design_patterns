package com.tomingdom.aop;

import com.tomingdom.dynamicProxy.Impl.Tourist;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AOPConfig {
    @Pointcut(value = "execution(public * com.tomingdom.dynamicProxy.Impl.ViewPoint.enter(..)) && args(tourist)")
    public void defaultPointCut(Tourist tourist) {
    }

    @Around(value = "defaultPointCut(tourist)")
    public void defaultAdvice(ProceedingJoinPoint proceedingJoinPoint, Tourist tourist) throws Throwable {
        if (tourist.isTicket() && tourist.isCanRecite()) {
            proceedingJoinPoint.proceed(new Object[]{tourist});
        } else {
            System.out.println("stop here");
        }
    }
}
