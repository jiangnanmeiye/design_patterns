package com.tomingdom.aop;

import com.tomingdom.dynamicProxy.Impl.Tourist;
import com.tomingdom.dynamicProxy.Impl.ViewPoint;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class AopApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(AopApplication.class, args);

        ViewPoint yellowCraneTower = ctx.getBean("yellowCraneTower", ViewPoint.class);
        Tourist tourist = new Tourist("刘子豪");
        tourist.readHuangHeLou();

        yellowCraneTower.enter(tourist);
    }

}
