package com.tomingdom.aop;

import com.tomingdom.dynamicProxy.Impl.ViewPoint;
import com.tomingdom.dynamicProxy.Impl.YellowCraneTower;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class ProjectConfig {

    @Bean
    @Lazy
    ViewPoint yellowCraneTower() {
        return new YellowCraneTower();
    }
}
