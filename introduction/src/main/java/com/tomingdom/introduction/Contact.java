package com.tomingdom.introduction;

import lombok.Data;

@Data
public class Contact {
    private String name;
    private String phoneNumber;
    private String email;
}
