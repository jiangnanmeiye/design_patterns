package com.tomingdom.introduction;

public interface IsModified {
    boolean isModified();
}
