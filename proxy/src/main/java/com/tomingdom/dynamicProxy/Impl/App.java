package com.tomingdom.dynamicProxy.Impl;

public class App {
    public static void main(String[] args) {
        Tourist tourist1 = new Tourist("刘子豪");
        tourist1.buyTicket();
        Tourist tourist2 = new Tourist("刘自豪");
        tourist2.buyTicket();
        tourist2.readHuangHeLou();

        YellowCraneTower yellowCraneTower = new YellowCraneTower();
        ViewPoint DynamicYellowCraneTowerProxy = (ViewPoint) DynamicProxy.getProxy(yellowCraneTower);
        System.out.println(tourist1.getName() + " wants visit");
        DynamicYellowCraneTowerProxy.enter(tourist1);
        System.out.println(tourist2.getName() + " wants visit");
        DynamicYellowCraneTowerProxy.enter(tourist2);

        System.out.println("-----------------------Leave-----------------------");

        DynamicYellowCraneTowerProxy.leave(tourist1);
        DynamicYellowCraneTowerProxy.leave(tourist2);

    }
}
