package com.tomingdom.dynamicProxy.Impl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class DynamicProxy implements InvocationHandler {
    private Object object;

    private DynamicProxy(Object object) {
        this.object = object;
    }

    @Override
    public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
        Object result = null;
        Tourist tourist = (Tourist) objects[0];
        if (tourist.isTicket() && tourist.isCanRecite()) {
            result = method.invoke(object, tourist);
        } else {
            System.out.println("stop here");
        }
        return result;
    }

    public static Object getProxy(ViewPoint viewPoint) {
        return Proxy.newProxyInstance(ViewPoint.class.getClassLoader(), new Class[]{ViewPoint.class}, new DynamicProxy(viewPoint));
    }
}
