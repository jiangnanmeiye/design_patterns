package com.tomingdom.dynamicProxy.Impl;

public interface ViewPoint {
    String name();

    void enter(Tourist tourist);

    void leave(Tourist tourist);
}
