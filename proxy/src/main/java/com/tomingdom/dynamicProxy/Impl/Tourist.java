package com.tomingdom.dynamicProxy.Impl;

public class Tourist {
    private boolean ticket = false;
    private boolean canRecite = false;
    private String name;

    public Tourist(String name) {
        this.name = name;
    }

    public void buyTicket() {
        ticket = true;
    }

    public void readHuangHeLou() {
        canRecite = true;
    }

    public boolean isCanRecite() {
        return canRecite;
    }

    public String getName() {
        return name;
    }

    public boolean isTicket() {
        return ticket;
    }
}
