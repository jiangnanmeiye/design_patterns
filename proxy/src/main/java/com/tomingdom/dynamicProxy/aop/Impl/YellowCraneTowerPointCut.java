package com.tomingdom.dynamicProxy.aop.Impl;

import com.tomingdom.dynamicProxy.Impl.Tourist;
import com.tomingdom.dynamicProxy.Impl.YellowCraneTower;
import org.springframework.aop.ClassFilter;
import org.springframework.aop.support.DynamicMethodMatcherPointcut;

import java.lang.reflect.Method;

public class YellowCraneTowerPointCut extends DynamicMethodMatcherPointcut {
    @Override
    public ClassFilter getClassFilter() {
        return cl -> cl == YellowCraneTower.class;
    }

    @Override
    public boolean matches(Method method, Class<?> targetClass) {
        return method.getName().equals("enter") || method.getName().equals("leave");
    }

    public boolean matches(Method method, Class<?> targetClass, Object... args) {
        return method.getName().equals("enter") || method.getName().equals("leave") || args[0] instanceof Tourist;
    }
}
