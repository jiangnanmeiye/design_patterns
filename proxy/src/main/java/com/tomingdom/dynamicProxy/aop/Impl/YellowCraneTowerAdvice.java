package com.tomingdom.dynamicProxy.aop.Impl;

import com.tomingdom.dynamicProxy.Impl.Tourist;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class YellowCraneTowerAdvice implements MethodInterceptor {

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Object object = null;
        Tourist tourist = (Tourist) invocation.getArguments()[0];
        if (tourist.isTicket() && tourist.isCanRecite()) {
            object = invocation.getMethod().invoke(invocation.getThis(), tourist);
        } else {
            System.out.println("stop here");
        }
        return object;
    }
}
