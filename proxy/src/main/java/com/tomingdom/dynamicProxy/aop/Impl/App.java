package com.tomingdom.dynamicProxy.aop.Impl;

import com.tomingdom.dynamicProxy.Impl.Tourist;
import com.tomingdom.dynamicProxy.Impl.ViewPoint;
import com.tomingdom.dynamicProxy.Impl.YellowCraneTower;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;

public class App {
    public static void main(String[] args) {
        ViewPoint yellowCraneTower = new YellowCraneTower();

        YellowCraneTowerPointCut pointCut = new YellowCraneTowerPointCut();
        YellowCraneTowerAdvice advice = new YellowCraneTowerAdvice();

        DefaultPointcutAdvisor advisor = new DefaultPointcutAdvisor(pointCut, advice);

        ProxyFactory pf = new ProxyFactory();
        pf.setInterfaces(ViewPoint.class);
        pf.addAdvisor(advisor);
        pf.setTarget(yellowCraneTower);

        ViewPoint yellowCraneTowerProxy = (ViewPoint) pf.getProxy();

        Tourist tourist = new Tourist("刘自豪");
        tourist.buyTicket();
        tourist.readHuangHeLou();

        Tourist tourist1 = new Tourist("刘子豪");
        tourist1.buyTicket();

        yellowCraneTowerProxy.enter(tourist);
        yellowCraneTowerProxy.enter(tourist1);
    }
}
