package com.tomingdom.staticProxy.Impl;

public interface ViewPoint {
    String name();

    void enter(Tourist tourist);

    void leave(Tourist tourist);
}
