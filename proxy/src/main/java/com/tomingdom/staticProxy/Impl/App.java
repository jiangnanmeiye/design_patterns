package com.tomingdom.staticProxy.Impl;

public class App {
    public static void main(String[] args) {
        Tourist tourist1 = new Tourist("刘子豪");
        tourist1.buyTicket();
        Tourist tourist2 = new Tourist("刘自豪");
        tourist2.buyTicket();
        tourist2.readHuangHeLou();

        YellowCraneTower yellowCraneTower = new YellowCraneTower();
        ViewPoint yellowCraneTowerProxy = new YellowCraneTowerProxy(yellowCraneTower);
        System.out.println(tourist1.getName() + " wants visit");
        yellowCraneTowerProxy.enter(tourist1);
        System.out.println(tourist2.getName() + " wants visit");
        yellowCraneTowerProxy.enter(tourist2);

        System.out.println("-----------------------Leave-----------------------");

        yellowCraneTowerProxy.leave(tourist1);
        yellowCraneTowerProxy.leave(tourist2);
    }
}
