package com.tomingdom.staticProxy.Impl;

public class YellowCraneTower implements ViewPoint {
    public String name() {
        return "Yellow Crane Tower";
    }

    public void enter(Tourist tourist) {
        System.out.println(tourist.getName() + "has entered!!!!");
    }

    public void leave(Tourist tourist) {
        System.out.println("Yeah! Happy day, " + tourist.getName() + ", let's go out");
    }
}
