package com.tomingdom.staticProxy.Impl;

public class YellowCraneTowerProxy implements ViewPoint {
    private final ViewPoint viewPoint;

    public YellowCraneTowerProxy(ViewPoint viewPoint) {
        this.viewPoint = viewPoint;
    }

    public String name() {
        return viewPoint.name();
    }

    public void enter(Tourist tourist) {
        if (tourist.isTicket() && tourist.isCanRecite()) {
            viewPoint.enter(tourist);
        } else {
            System.out.println("stop here");
        }
    }

    public void leave(Tourist tourist) {
        if (tourist.isTicket() && tourist.isCanRecite()) {
            viewPoint.leave(tourist);
        } else {
            System.out.println("stop here");
        }
    }
}
