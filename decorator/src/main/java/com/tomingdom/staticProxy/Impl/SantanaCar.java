package com.tomingdom.staticProxy.Impl;

public class SantanaCar implements Car {
    @Override
    public void drive() {
        System.out.println("Oh! Fastest speed: 180Km/h");
    }
}
