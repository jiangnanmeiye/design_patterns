package com.tomingdom.staticProxy.Impl;

public class AMGMercedesCar extends MercedesCar {
    public AMGMercedesCar(Car car) {
        super(car);
    }

    public void speedUp() {
        System.out.println("Oh! It can Speed up to 200Km/h");
    }

    @Override
    public void drive() {
        super.drive();
        speedUp();
    }
}
