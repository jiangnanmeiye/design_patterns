package com.tomingdom.staticProxy.Impl;

public class G63MercedesCar extends MercedesCar {
    public G63MercedesCar(Car car) {
        super(car);
    }

    public void raiseBody() {
        System.out.println("Raise the body by 20 cm");
    }

    @Override
    public void drive() {
        raiseBody();
        super.drive();
    }
}
