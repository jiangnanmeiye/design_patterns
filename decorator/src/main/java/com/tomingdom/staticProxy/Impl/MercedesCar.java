package com.tomingdom.staticProxy.Impl;

public abstract class MercedesCar implements Car {
    private Car car;

    public MercedesCar(Car car) {
        this.car = car;
    }

    @Override
    public void drive() {
        car.drive();
    }
}
