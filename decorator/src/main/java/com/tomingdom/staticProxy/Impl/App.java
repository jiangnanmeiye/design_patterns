package com.tomingdom.staticProxy.Impl;

import com.tomingdom.SplitLine;

public class App {
    public static void main(String[] args) {
        SantanaCar santanaCar = new SantanaCar();

        SplitLine.drawSplitLine("AMGMercedes");

        AMGMercedesCar amgMercedesCar = new AMGMercedesCar(santanaCar);
        amgMercedesCar.drive();

        SplitLine.drawSplitLine("G63Mercedes");

        G63MercedesCar g63MercedesCar = new G63MercedesCar(amgMercedesCar);
        g63MercedesCar.drive();
    }
}
