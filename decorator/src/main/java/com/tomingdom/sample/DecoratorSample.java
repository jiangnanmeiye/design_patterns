package com.tomingdom.sample;

import com.tomingdom.SplitLine;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class DecoratorSample {
    public static void main(String[] args) {
        Path file = Paths.get("decorator/ubuntu-19.10-desktop-amd64.iso");

        try (InputStream in = Files.newInputStream(file, StandardOpenOption.READ)) {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(in);
            //bytes length smaller, BufferedInputStream faster
            byte[] bytes = new byte[1024];

            long begin = System.currentTimeMillis();
            int times = 0;
            while (bufferedInputStream.read(bytes) != -1) {
                times++;
            }
            long end = System.currentTimeMillis();

            System.out.printf("use %-2d seconds, read %-8d times\n", (end - begin) / 1000, times);

            SplitLine.drawSplitLine("FileInputStream Analysis");

            InputStream in2 = Files.newInputStream(file, StandardOpenOption.READ);
            begin = System.currentTimeMillis();
            times = 0;
            while (in2.read(bytes) != -1) {
                times++;
            }
            end = System.currentTimeMillis();

            System.out.printf("use %-2d seconds, read %-8d times", (end - begin) / 1000, times);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
