package com.tomingdom.sample;

import com.tomingdom.SplitLine;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class Input {
    public static void main(String[] args) {
        Path file = Paths.get("decorator/test.txt");

        try (OutputStream out = Files.newOutputStream(file, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
             InputStream in = Files.newInputStream(file, StandardOpenOption.READ)) {

            //写的是十进制的Unicode编码
            //这是汉字‘刘’的Unicode编码,其16进制为Ox5218,十进制为21016,对于超过一个字节大小的字符就不能用参数为字节的那个方法
            out.write(new byte[]{82, 24});

            byte[] ch1 = new byte[2];
            in.read(ch1);
            System.out.println((char) ((ch1[0] << 8) + ch1[1]));

            SplitLine.drawSplitLine("DataOutputStream/DataInputStream Analysis");

            //智能的写入
            DataOutputStream dataOutputStream = new DataOutputStream(out);
            dataOutputStream.writeChar(21016);

            DataInputStream dataInputStream = new DataInputStream(in);
            char ch = dataInputStream.readChar();
            System.out.println(ch);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
